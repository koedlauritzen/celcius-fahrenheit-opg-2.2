﻿using System;

namespace ConsoleUI
{
    class Program
    {
        private static string Temperaturskala { get; set; }
        private static float Temperatur { get; set; }
        private static float OmregnetTemperatur { get; set; }

        static void Main(string[] args)
        {
            Temperaturskala = VaelgTemperaturskala();
            Temperatur = IndtastTemperatur();
            OmregnetTemperatur = OmregnTemperatur(Temperaturskala, Temperatur);

            string modsatTemperaturSkala = (Temperaturskala == "C") ? "Fahrenheit" : "Celsius";
            Console.WriteLine($"Omregnet temperatur: { OmregnetTemperatur.ToString("F1") } { modsatTemperaturSkala } ");
            Console.ReadKey();
        }

        private static string VaelgTemperaturskala()
        {
            string brugerTempSkala;
            do
            {
                Console.WriteLine("Vælg Temperaturskala: [C]elsius [F]ahrenheit");
                brugerTempSkala = Console.ReadLine().ToUpper();
            } while (brugerTempSkala != "C" && brugerTempSkala != "F");

            return brugerTempSkala;
        }

        private static float IndtastTemperatur()
        {
            float brugerTemp;

            Console.WriteLine($"Indtast temperatur du ønsker at omregne:");
            while (!float.TryParse(Console.ReadLine(), out brugerTemp))
            {
                Console.WriteLine("Du skal indtaste et tal");
            }

            return brugerTemp;
        }

        private static float OmregnTemperatur(string temperaturSkala, float temperatur)
        {
            float omregnetTemperatur = 0;

            switch (temperaturSkala)
            {
                case "C":
                    omregnetTemperatur = (temperatur * 1.8F) + 32;
                    break;

                case "F":
                    omregnetTemperatur = (temperatur - 32) * .5556F;
                    break;
            }

            return omregnetTemperatur;
        }
    }
}
